///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.h
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

extern int addCat(char* inputname, enum genders inputgender, enum breeds inputbreed, bool isFixed, float inputweight);
