///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "updateCats.h"

void updateCatName(int index, char* newName)
{
    for (int i = 0; i < MAX_CATS; i++){ //Checks if name is in database
        indatabase = strcmp(database[i].name, newName);
        if (indatabase == 0){
        break;
        }
    }
    if (indatabase != 0) {
        for (int j = 0; j < MAX_CAT_NAME; j++){
            database[index].name[j] = newName[j];
        }
    }
    else{
        printf("NAME ALREADY TAKEN\n");
    }
}

void fixCat(int index)
{
    database[index].isFixed = true;
}

void updateCatWeight(int index, float newWeight)
{
    if (newWeight > 0){ //Checks if weight is > 0
        database[index].weight = newWeight;
    }
}

