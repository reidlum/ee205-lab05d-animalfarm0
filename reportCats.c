///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "reportCats.h"

void printCat(int index)
{
    if (index < MAX_CATS && index >= 0) //Checks if index is allowed
    {
        printf("cat index=%d name=%s gender=%d breed=%d isFixed=%d weight=%f\n",index,database[index].name,database[index].gender,database[index].breed,database[index].isFixed,database[index].weight);
    }
    else{
        printf("animalFarm0: Bad cat [%d]\n",index);
    }
}

void printAllCats()
{
    for(int i = 0; i < MAX_CATS; i++){
        if (database[i].weight != 0){ //Just here so it doesn't print like 90 something blank cat structs
            printf("cat index=%d name=%s gender=%d breed=%d isFixed=%d weight=%f\n",i,database[i].name,database[i].gender,database[i].breed,database[i].isFixed,database[i].weight);
        }
            
    }
}

int findCat(char* searchname)
{
    for (int i = 0; i < MAX_CATS; i++){ //Checks if name is in database
        find = strcmp(database[i].name, searchname);
        if (find == 0){
            printf("cat index=%d\n",i);
            return i;
            break;
        }
        else if (i == MAX_CATS-1 && find !=0){
            printf("CAT CAN'T BE FOUND\n");
            return 500;
        }
    }
    return 0;
}
