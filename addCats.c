///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "catDatabase.h"
#include "addCats.h"

int addCat(char* inputname, enum genders inputgender, enum breeds inputbreed, bool isFixed, float inputweight)
{
   if (numcats != MAX_CATS) //Checks if database is full
   {
      if (strlen(inputname) > 0) //Checks if cat name is empty
      {
         if (strlen(inputname) < 30) //Checks if cat name is < 30
         {
            if (inputweight > 0) //Checks if weight is > 0
            {
                for (int i = 0; i < MAX_CATS; i++){ //Checks if name is in database
                    indatabase = strcmp(database[i].name, inputname);
                    if (indatabase == 0){
                        break;
                    }
                }
                if (indatabase != 0) {
                    for (int j = 0; j < MAX_CAT_NAME; j++){
                        database[numcats].name[j] = inputname[j];
                    }
                    database[numcats].gender = inputgender;
                    database[numcats].breed = inputbreed;
                    database[numcats].isFixed = isFixed;
                    database[numcats].weight = inputweight;
                    numcats = numcats + 1;
                    return numcats - 1;
                }
                else{
                    printf("NAME IS IN THE DATABASE U DUMMY\n");
                    return 500;
                }

            }
            else{
                printf("CATS ARE ALWAYS FAT SO PUT A WEIGHT\n");
                return  500;
            }
         }
         else{
            printf("WHO NAMES THEIR CAT SOMETHING THAT LONG\n");
            return  500;
         }
      }
      else{
          printf("WHAT'S YOUR CATS NAME BRO\n");
          return  500;
      }
   }
   else{
       printf("TOO MANY CATS CHILL OUT\n");
       return 500;
   }
}
