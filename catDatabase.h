///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Reid Lum <reidlum@hawaii.edu>
/// @date 14_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#define MAX_CATS     (100)
#define MAX_CAT_NAME (32)
enum genders {UNKNOWN_GENDER, MALE, FEMALE};
enum breeds {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};

struct Cat{
    char name[MAX_CAT_NAME];
    enum genders gender;
    enum breeds breed;
    bool isFixed;
    float weight;
};

extern struct Cat database[MAX_CATS];

extern int numcats;
extern int indatabase;
extern int find;
